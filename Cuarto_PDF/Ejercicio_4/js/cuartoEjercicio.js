/** Dado el siguiente código: */

let zero = 0;
function multiply(x) {return x * 2;}
function add(a =1 + zero, b = a, c = b + a, d = multiply(c)) {
    console.log('Ejemplo: ',(a + b + c), d);
}
add();

/** Cual es el resultado de ejecutar las siguientes sentencias? */

//add(1);
//add(3);
//add(2, 7);
//add(1, 2, 5);
//add(1, 2, 5, 10);

//Primero con add(1)
function add1(a =1 + zero, b = 1, c = b + a, d = multiply(c)) {
    console.log('Primer resultado: ',(a + b + c), d); //Resultado: 4 4
}
add1();

//Segundo add(3)
function add2(a =3 + zero, b = a, c = b + a, d = multiply(c)) {
    console.log('Segundo resultado: ',(a + b + c), d); //Resultado: 12 12
}
add2();

//Tercer add(2,7)
function add3(a = 2 + zero, b = 7, c = b + a, d = multiply(c)) {
    console.log('Tercer resultado: ',(a + b + c), d); //Resultado: 18 18
}
add3();

//Cuarto add(1,2,5)
function add4(a =1 + zero, b = 2, c = 5 + a, d = multiply(c)) {
    console.log('Cuarto resultado: ',(a + b + c), d); //Resultado: 9 12
}
add4();

//Quinto add(1,2,5,10)
function add5(a =1 + zero, b = 2, c = 5 + a, d = 10) {
    console.log('Quinto resultado: ',(a + b + c), d); //Resultado: 9 10
}
add5();