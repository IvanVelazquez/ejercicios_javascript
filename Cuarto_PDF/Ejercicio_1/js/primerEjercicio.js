/** Dado el siguiente objeto y arreglo */
var anObject = {
    foo: 'bar',
    length: 'interesting',
    '0': 'zero!',
    '1': 'one'
};

var anArray = ['zero', 'one.'];

/** Cual es el resultado del siguente código y ¿por que? */

console.log(anArray[0], anObject[0]); /** Aqui estamos imprimiendo el contenido de nuestro
arreeglo y objeto, espesificamente los elementos que se encuentran en la primera posisción 
o que tengan asignado un id numerico, en el arreglo el valor de nuestro elemento con el atributo
identificador es '0' con el valor asignado: zero!, por eso es que nos imprime esto, y en el objeto
no se define un valor de atributo, simplemente se toma la posicion de este elemento que siguendo 
las normas de los operadores en programaciòn es 0*/

console.log(anArray[1], anObject[1]); /** Aqui mostramos el valor de nuestro elemento contenido en
el arreglo con la posición 1 el cual es  'one.' ya que partimos del 0, en nuestro objeto mostramos 
el elemento que se encuentra con el identificador atributo '1'*/

console.log(anArray.length, anObject.length); /** Aqui estamos usando la propiedad '.length' en 
nuestro arreglo para contar el numero de elemnentos que este contiene, por el caso contrario, en el 
objeto tenemos un  valor asignado para la propidad '.length' por lo cual aqui muestra su valor en 
lugar de realizar el conteo de elementos contenidos en nuestro objeto*/

console.log(anArray.foo, anObject.foo); /** Aqui se intenta imprimir la propiedad foo de nuestro arreglo,
en el cual no tenemos definido ningun valor para poder imprimir, por esto, el valor es undefinined, 
por el caso contrario en nuestro objeto si contamos con un valor asignado a esta propiedad por lo cual 
se nos imprime este valor 'bar'*/

console.log(typeof anArray == 'object', typeof anObject == 'object'); /** Aqui comparamops 
si nuestro arreglo  es igual a un objeto y si nuestro objeto es un 'objeto' si esto es verdadero como en 
este caso se imprime un valor boleando true en ambos casos*/

console.log(anArray instanceof Object, anObject instanceof Object); /** Aqui varificamos si nuestro arreglo
contienen un objeto y si nuestro objeto contienen o es un objeto, el cual en ambos casos es verdadero y se 
imprime nuevamente los valores booleanos 'true' y 'true'*/

console.log(anArray instanceof Array, anObject instanceof Array); /** Verificamos si nuestro arreglo 
es un arreglo o contiene un arreglo el cual en este caso es verdadero ya que es un arreglo, por el caso 
contrario verificamos si nuestro objeto es un arreglo o su contiene un arreglo, el cual en este caso es falso 
por lo cual imprimimos y obtenemos nuestros resaultados que son: 'true' y 'false'*/

console.log(Array.isArray(anArray), Array.isArray(anObject)); /** Nuevamente utilizando otro metodo verificamos 
si nuestro anArray es un arreglo, el cual en este caso es verdadero y si nuestro anObjet es un arreglo, el 
cual en este caso es falso ya que es un objeto, por lo cual se imprimen los resultados booleanos 'true' y 'false'*/

