//Dado el siguiente código:

class Queue {
    constructor() {
        const list = [];

        this.enqueue = function (type) {
            list.push(type);
            return type;
        };
        this.dequeue = function () {
            return list.shift();
        };
    }
}

//Cual es el resultado de ejecuta las siguientes sentencias y ¿porque?:
 var q = new Queue;

 q.enqueue(9);
 q.enqueue(8);
 q.enqueue(7);

console.log(q.dequeue());
console.log(q.dequeue());
console.log(q.dequeue());/** Esto nos esta mostrando los valores en un metodo de tipo pila,
 * es decir, colando los datos 
 * para el metodo 'enqueue' los datos en el orden que se definieron
 */

console.log(q); /** Aqui nos esta imprimendo los elementos de el arreglo que se formo con su 
 *key y su valor */
console.log(Object.keys(q)); /** Aqui imprimimos solo las propiedades key de nuestro arreglo
 * es decir, nuestro 'enqueue' y 'dequeue'*/

