//Dado el siguiente código

class MyClass {
    constructor() {
        this.names_ = [];
    }

    set name(value) {
        this.names_.push(value);
    }

    get name() {
        return this.names_[this.names_.length - 1];
    }
}

const myClassInstance = new MyClass();
myClassInstance.name = 'Joe';
myClassInstance.name = 'Bob';

//Cual es el resultado de ejecutar las siguientes sentencias y ¿Por que?: 
console.log(myClassInstance.name); /** Aqui esta mostrando el resultado de nuestra function get
donde retornamos el valor de nuestro arreglo menos el primer elemnto de nuestro arreglo */

console.log(myClassInstance.names_); /** Aqui se imprime el contenido de nuestro arreglo que en 
este caso son los nombres que agregamos en nuestra clase */