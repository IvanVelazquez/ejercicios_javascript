// Dado el siguiente objeto:
var obj = {
    a: "hello",
    b: "this is", 
    c: "javascript!"
};

//Que metodo del objeto 'Object' puedo usar para que me dé el siguiente resultado:
//console.log((array)); //"hello", "this is", "javascript!

array = Object.values(obj);

console.log(array);