//Dado el siguiente código

const classInstance = new class {
    get prop() {
        return 5;
    }
};
//Cual es el resultado de ejecutar las siguientes sentencias y ¿Por que?
classInstance.prop = 10;

console.log(classInstance.prop); //logs: 5
/** Aqui nos imprime el valor de 5 ya que en nuestra clase se ha establecido 
 * que al llamar la prop de nuestra clase el valor retornado sera siempre 5
 */