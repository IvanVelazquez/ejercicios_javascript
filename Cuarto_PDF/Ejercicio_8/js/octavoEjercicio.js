//Dada la siguiente clase

class Person {
    constructor() {
        this.firstName = [];
        //this.lastName = [];
    }

    set firstName(value) {
        this.firstName.push(value);
    }

    get firstName(){
        return this.firstName[this.firstName.length - 1];
    }

    set lastName(value) {
        this.lastName.push(value);
    }

    get lastName() {
        return this.lastName[this.lastName.length - 1];
    }

}

const PersonInstance = new Person();
PersonInstance.firstName = 'Foo';
PersonInstance.lastName = 'Bar';
/** Define los metodos correspondientes 
 * para ejecutar el siguente código: */

let person = new Person('John', 'Doe');

console.log(person.firstName, person.lastName);

person.firstName = 'Foo';
person.lastName = 'Bar';

console.log(person.firstName, person.lastName);